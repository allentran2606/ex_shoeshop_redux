import React, { Component } from "react";
import { connect } from "react-redux";
import { CHANGE_QTY } from "./redux/constant/constant";

class Cart extends Component {
  renderTbody = () => {
    return this.props.cart.map((item) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price * item.number}</td>
          <td>
            <button
              className="btn btn-success mr-1"
              onClick={() => {
                this.props.changeQty(item.id, -1);
              }}
            >
              <i className="fa fa-angle-left"></i>
            </button>
            <strong>{item.number}</strong>
            <button
              className="btn btn-success ml-1"
              onClick={() => {
                this.props.changeQty(item.id, +1);
              }}
            >
              <i className="fa fa-angle-right"></i>
            </button>
          </td>
          <td>
            <img style={{ width: "80px" }} src={item.image} alt="true" />
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <table className="table">
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Price</th>
            <th>Quantity</th>
            <th>Image</th>
          </tr>
        </thead>
        <tbody>{this.renderTbody()}</tbody>
      </table>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    cart: state.shoeReducer.cart,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    changeQty: (id, value) => {
      dispatch({
        type: CHANGE_QTY,
        payload: id,
        value: value,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
